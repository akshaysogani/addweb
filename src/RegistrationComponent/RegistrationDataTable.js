import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Label, Table} from 'semantic-ui-react'
import {Link} from 'react-router-dom'

class RegistrationDataTable extends Component {
    constructor(props) {
        super(props);

    }

// function to show the confirm box dialog and navigate the page
    edit = () => {
        const result = window.confirm("Are you want to edit ?");
        if (result) {
            this.props.dispatch({type: 'EDIT', id: this.props.post.id});
            this.props.history.push(`/registration/${this.props.post.id}`);
        }

    };

// function to show the confirm box dialog and delete the data
    deleteData = () => {
        const result = window.confirm("Are you want to delete ?");
        if (result) {
            this.props.dispatch({type: 'DELETE', id: this.props.post.id})
        }

    };

    render() {

        return (
            <div>
                <Table celled>
                    <Table.Header style={{display: this.props.post.id === 0 ? '' : "none"}}>
                        <Table.Row>
                            <Table.HeaderCell>Photo</Table.HeaderCell>
                            <Table.HeaderCell>First Namae</Table.HeaderCell>
                            <Table.HeaderCell>Last Name</Table.HeaderCell>
                            <Table.HeaderCell>Father Name</Table.HeaderCell>
                            <Table.HeaderCell>Email</Table.HeaderCell>
                            <Table.HeaderCell>Sex</Table.HeaderCell>
                            <Table.HeaderCell>Address</Table.HeaderCell>
                            <Table.HeaderCell>Date of birth</Table.HeaderCell>
                            <Table.HeaderCell>Country</Table.HeaderCell>
                            <Table.HeaderCell>Actions</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        <Table.Row>
                            <Table.Cell>
                                <img src={this.props.post.registerFormData.firstName} width="100" height="100"/>
                            </Table.Cell>
                            <Table.Cell>
                                <Label>{this.props.post.registerFormData.firstName}</Label>
                            </Table.Cell>
                            <Table.Cell><Label>{this.props.post.registerFormData.lastName}</Label></Table.Cell>
                            <Table.Cell><Label>{this.props.post.registerFormData.fatherName}</Label></Table.Cell>
                            <Table.Cell><Label>{this.props.post.registerFormData.email}</Label></Table.Cell>
                            <Table.Cell><Label>{this.props.post.registerFormData.sex}</Label></Table.Cell>
                            <Table.Cell><Label>{this.props.post.registerFormData.address}</Label></Table.Cell>
                            <Table.Cell><Label>{this.props.post.registerFormData.dateOfBirth}</Label></Table.Cell>
                            <Table.Cell><Label>{this.props.post.registerFormData.country}</Label></Table.Cell>
                            <Table.Cell>
                                <button onClick={this.edit}>Edit</button>
                                <button onClick={this.deleteData}>
                                    Delete
                                </button>
                                <button  ><Link to="/"> Add new Registration</Link></button>
                            </Table.Cell>
                        </Table.Row>
                    </Table.Body>
                    <Table.Footer>
                        <Table.Row>
                            <Table.HeaderCell colSpan='10'>
                            </Table.HeaderCell>
                        </Table.Row>
                    </Table.Footer>
                </Table>
            </div>
        );
    }
}
export default connect()(RegistrationDataTable);