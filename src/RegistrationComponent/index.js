import React, {Component} from 'react';
import {connect} from 'react-redux';
import store from '..';
import {Form, Radio, Input, Button, TextArea, Popup, Dropdown} from 'semantic-ui-react'

var storeData;
const countryOptions = [
    {key: 'af', value: 'Afghanistan', text: 'Afghanistan'},
    {key: 'al', value: 'Albania', text: 'Albania'},
    {key: 'dz', value: 'Algeria', text: 'Algeria'},
    {key: 'ad', value: 'Andorra', text: 'Andorra'},
    {key: 'ao', value: 'Angola', text: 'Angola'},
    {key: 'ai', value: 'Anguilla', text: 'Anguilla'},
    {key: 'ag', value: 'Antigua', text: 'Antigua'},
    {key: 'ar', value: 'Argentina', text: 'Argentina'},
    {key: 'am', value: 'Armenia', text: 'Armenia'},
    {key: 'aw', value: 'Aruba', text: 'Aruba'},
    {key: 'au', value: 'Australia', text: 'Australia'}
];

class Registration extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        console.log(store);
        storeData = store ? store.getState() : [];
        if (storeData.length > 0 && props.match.params.id) {
            const matchedPost = storeData[props.match.params.id];
            if (matchedPost) {
                this.state = matchedPost['registerFormData'];
                // const selectedCountry = countryOptions.find(function (element) {
                //     return element.value === matchedPost['registerFormData'].country;
                // });
                // this.state.country= selectedCountry;
                // console.log(this.state)
            }
        }

    }

    // check form is valid or not
    isFormValid = () => {
        if (!this.state.firstName || !this.state.lastName || !this.state.fatherName || !this.state.address
            || !this.state.mobileNumber || !this.state.email || !this.state.dateOfBirth || !this.state.sex
            || !this.validateEmail(this.state.email) || !this.state.country) {
            return false
        } else {
            return true;
        }
    };

    // check email type
    validateEmail = (email) => {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        debugger;
        return re.test(email)

    };

    handleSubmit = (e) => {
        if (this.isFormValid()) {
            debugger;
            e.preventDefault();
            const registerFormData = this.state;
            const data = {
                id: this.props.match.params.id ? this.props.match.params.id :
                    storeData.length,
                registerFormData: registerFormData,
                editing: false
            };
            if (this.props.match.params.id) {
                this.props.dispatch({
                    type: 'UPDATE',
                    data
                })
            } else {
                this.props.dispatch({
                    type: 'ADD',
                    data
                })
            }
            debugger;
            this.props.history.push("/Registration-list");
        } else {

        }

    };

    // changes event handlers
    handleChange = (event) => {
        this.setState({[event.target.name]: event.target.value});
    };

    radioButtonHandlerChange = (e, {value}) => this.setState({sex: value});

    selectChangeHandler = (value) => {
        this.setState({'country': value})
    };
    fileUploadHandler = (event) => {
        this.setState({'files': event.target.files});
    };

    render() {
        return (
            <div style={{margin: '20px', width: '312px', textAlign: 'center'}}>
                <h1>Registration </h1>
                <Form onChange={this.handleChange}>
                    <Form.Field>
                        <Popup
                            trigger={<Input type="file" name="files" text={this.state.files} multiple
                                            onChange={(event) =>
                                                this.fileUploadHandler(event)
                                            }
                                            required="true"/>}
                            content='Upload your pic.'
                            on='hover'

                        />
                    </Form.Field>
                    <Form.Field>
                        <Popup
                            trigger={<Input type="text" placeholder="first Name" name="firstName"
                                            value={this.state.firstName}
                                            required="true"/>}
                            content='First Name is mandatory.'
                            on='hover'
                        />
                    </Form.Field>
                    <Form.Field>
                        <Popup
                            trigger={<Input type="text" placeholder="Last Name" name="lastName"
                                            value={this.state.lastName}
                                            required/>}
                            content='Last Name is mandatory.'
                            on='hover'
                        />


                    </Form.Field>
                    <Form.Field>
                        <Popup
                            trigger={<Input type="text" placeholder="Father Name" name="fatherName"
                                            value={this.state.fatherName}
                                            required/>}
                            content='Father Name is mandatory.'
                            on='hover'
                        />

                    </Form.Field>
                    <Form.Field>
                        <Popup
                            trigger={<TextArea type="text" placeholder="Address" name="address"
                                               value={this.state.address} required/>}
                            content='Address is mandatory.'
                            on='hover'
                        />

                    </Form.Field>
                    <Form.Field>
                        <Popup
                            trigger={<Input type="number" placeholder="Mobile Number" name="mobileNumber" required
                                            value={this.state.mobileNumber}/>}
                            content='Mobile number is mandatory.'
                            on='hover'
                        />

                    </Form.Field>
                    <Form.Field>
                        <Popup
                            trigger={<Input type="email" placeholder="email" name="email" value={this.state.email}
                                            required/>}
                            content='Email is mandatory.'
                            on='hover'
                        />

                    </Form.Field>
                    <Form.Field>
                        <Popup
                            trigger={<Input type="date" placeholder="DOB" name="dateOfBirth"
                                            value={this.state.dateOfBirth}
                                            required/>}
                            content='Dob is mandatory.'
                            on='hover'
                        />
                    </Form.Field>
                    <Form.Field>
                        <Radio
                            label='Male'
                            name='radioGroup'
                            value='male'
                            checked={this.state.sex === 'male'}
                            onChange={this.radioButtonHandlerChange}
                        />
                    </Form.Field>
                    <Form.Field>
                        <Radio
                            label='Female'
                            name='radioGroup'
                            value='female'
                            checked={this.state.sex === 'female'}
                            onChange={this.radioButtonHandlerChange}
                        />
                    </Form.Field>
                    <Form.Field>
                        <Radio
                            label='Other'
                            name='radioGroup'
                            value='other'
                            checked={this.state.sex === 'other'}
                            onChange={this.radioButtonHandlerChange}
                        />
                    </Form.Field>
                    <Form.Field>
                        <Popup
                            trigger={
                                <Dropdown fluid search selection
                                          placeholder='Select your country' options={countryOptions}
                                          onChange={(e, data) =>
                                              this.selectChangeHandler(data.value)
                                          }
                                />
                            }
                            content='Country is mandatory.'
                            on='hover'
                        />

                    </Form.Field>
                    <Button onClick={this.handleSubmit}> submit </Button>
                </Form>

            </div>
        );
    }
}
export default connect()(Registration);