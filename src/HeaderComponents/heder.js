import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom'
class Header extends Component {

    render() {
        return (
            <div style={{float:'right', background:'blue'}}>
                <button  ><Link to="/"> Add new Registration</Link> </button>
                <button  ><Link to="/Registration-list"> Registration List</Link> </button>
            </div>
        );
    }
}

export default connect()(Header);