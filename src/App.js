import React from 'react';
import './App.css';
import {createBrowserHistory} from 'history';
import {Route} from 'react-router-dom';
import {Router} from 'react-router'
import Registration from "./RegistrationComponent/index";
import RegistrationList from "./ListComponent/index";
import Header from"./HeaderComponents/heder"

function App() {
    const history = createBrowserHistory();
    return (
        <div className="App">
            <div className="navbar">
            </div>
            <Router history={history}>
                <Header/>
                <Route exact path="/" component={Registration} />
                <Route exact path="/registration" component={Registration}/>
                <Route exact path="/registration-list" component={RegistrationList}/>
                <Route exact path="/registration/:id" component={Registration}/>
            </Router>
        </div>

    );

}

export default App;
