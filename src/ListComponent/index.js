import React, {Component} from 'react';
import {connect} from 'react-redux';
import RegistrationDataTable from "../RegistrationComponent/RegistrationDataTable";

class RegistrationList extends Component {
    render() {
        return (
            <div>
                <h1>Views</h1>
                {this.props.registeredData.map((reg) => (
                    <div key={reg.id}>
                        <RegistrationDataTable post={reg} key={reg.id} history={this.props.history}/>
                    </div>
                ))}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        registeredData: state
    }
};
export default connect(mapStateToProps)(RegistrationList);